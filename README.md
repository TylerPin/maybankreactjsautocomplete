#  Google Map with Autocomplete

# Setting up web

---

In the project root, 

1. ``yarn install`` 

2. ``yarn run`` 

3. Open http://localhost:8080/ on your browser

Due to Google API's client limitation, your browser might need to disable CORS. 

You can disable it using these extension: 

1. Google Chrome - https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino?hl=en
2. Firefox - https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/

# Walkthrough
1. Insert API key on the top right text box

2. Type in any location you would like to search

3. Click on the result and see it populate on the map

4. Clear the result and you'll see your past selected history

5. To test on failed scenario, remove the API key and search
# Features
### Higher Order Component
* Generic components such as loading, success / error prompts can be grouped inside here.
* In this sample, I utilise HOC to display error messages. 
### Redux Saga
* I use Redux saga as my middleware.
* For this sample, I utilise saga for API calling
### Material Design Kit
* In this sample I use material Autocomplete and TextField, as well as error banner.
### Higher Order Functions
* In this sample I use built in .filter to remove duplicate search history.

