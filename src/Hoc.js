import React, { Component } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { connect } from 'react-redux';
import { apiError } from './redux/actions'

@connect((state) => ({
  errorMsg: state.errorMsg,
}), {
  apiError
})

export default function Hoc(HocComponent) {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isShowAlert: this.props.errorMsg ? true : false,
      }
    }

    componentDidUpdate(prevProps) {
      if (prevProps.errorMsg !== this.props.errorMsg) {
        this.setState({ isShowAlert: true })
      }
    }

    handleClose1 = () => {
      this.setState({
        isShowAlert: false
      })
    }

    onExited = () => {
      this.props.apiError('')
    }

    render() {
      return (
        <div>
          <HocComponent></HocComponent>
          {this.props.errorMsg &&
            <Snackbar open={this.state.isShowAlert} onClose={this.handleClose1} onExited={this.onExited} autoHideDuration={1500} >
              <Alert severity="error">
                {this.props.errorMsg}
              </Alert>
            </Snackbar>
          }
        </div>

      );
    }
  }
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
