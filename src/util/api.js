const axios = require('axios');
import store from '../redux/store'
// console.log(store)

export default {
  getSuggestion: async function (data) {
    return new Promise(async (resolve, reject) => {
      try {
        let config = {
          method: 'get',
          url: 'https://maps.googleapis.com/maps/api/place/autocomplete/json',
          params: data,
        }
        let result = await this.axiosStart(config)
        resolve(result)
      }
      catch (e) {
        reject(e)
      }
    })
  },
  getPlaceById: async function (data) {
    return new Promise(async (resolve, reject) => {
      try {
        let config = {
          method: 'get',
          url: 'https://maps.googleapis.com/maps/api/place/details/json',
          params: data,
        }
        let result = await this.axiosStart(config)
        resolve(result)
      }
      catch (e) {
        reject(e)
      }
    })
  },
  axiosStart: async function (config) {
    return new Promise(async (resolve, reject) => {
      try {
        config.params['key'] = store.getState().api_key
        let response = await axios(config)
        if (response.data.error_message) {
          console.log('%cAxios failed', 'color: red;font-weight: bold;', response)
          throw (response.data.error_message)
        }
        console.log('%cAxios success', 'color: green;font-weight: bold;', response)
        resolve(response.data)
      }
      catch (e) {
        if (e.message) {
          reject(e.message)
        }
        reject(JSON.stringify(e))
      }
    })
  },
}

// AIzaSyCS2skggXWdR4tAYQWFquK1Bi6IcAhaEJw