import * as actionTypes from './actionTypes'

export function updateHistoryList(data) {
  return {
    type: actionTypes.UPDATE_HISTORY_LIST,
    historyList: data
  }
}

export function resetReduxData() {
  return {
    type: actionTypes.RESET_REDUX_DATA
  }
}

export function setSelectedPlace(data) {
  return {
    type: actionTypes.SET_SELECTED_PLACE,
    selectedPlace: data
  }
}

export function setApiKey(data) {
  return {
    type: actionTypes.SET_API_KEY,
    api_key: data
  }
}

export function apiError(data) {
  return {
    type: actionTypes.API_ERROR,
    errorMsg: data
  }
}