import * as actionTypes from './actionTypes'
import { put, takeLatest, all, select } from 'redux-saga/effects'
import API from '../util/api'

function* getPlaces() {
  try {
    let placeid = yield select((state) => state.selectedPlace.place_id);
    const data = yield API.getPlaceById({ placeid })
    yield put({ type: actionTypes.SET_SELECTED_PLACE_DATA, selectedPlaceData: data.result })
  } catch (e) {
    yield put({ type: actionTypes.API_ERROR, e })
  }
}
function* dataSaga() {
  yield takeLatest(actionTypes.SET_SELECTED_PLACE, getPlaces)
}

export default function* rootSaga() {
  yield all([
    dataSaga(),
  ]);
}
