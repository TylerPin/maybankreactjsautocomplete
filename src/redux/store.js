import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import reducer from './reducers';
import rootSaga from './sagas';
const persistedState = localStorage.getItem('reduxState')
  ? JSON.parse(localStorage.getItem('reduxState'))
  : {}

const store = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    reducer,
    persistedState,
    applyMiddleware(
      createLogger(),
      sagaMiddleware,
    )
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export default store()