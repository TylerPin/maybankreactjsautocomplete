import * as actionTypes from './actionTypes'

const initialState = {
  api_key: "",
  historyList: [{
    description: "Maybank Tower, Jalan Tun Perak, Bukit Bintang, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia",
    id: "1db0f50ffbaff36263f403b927c34fda416b7c3e",
    place_id: "ChIJwcT1wtNJzDERK72Wi2Bzthc",
    reference: "ChIJwcT1wtNJzDERK72Wi2Bzthc",
  }],

  selectedPlace: {},

  /** API */
  errorMsg: '',
  selectedPlaceData: {},

};

export default (state = initialState, action) => {
  switch (action.type) {

    /** Actions */
    case actionTypes.SET_API_KEY: {
      return {
        ...state,
        api_key: action.api_key
      }
    }
    case actionTypes.UPDATE_HISTORY_LIST: {
      return {
        ...state,
        historyList: action.historyList
      }
    }
    case actionTypes.SET_SELECTED_PLACE: {
      return {
        ...state,
        selectedPlace: action.selectedPlace
      }
    }
    case actionTypes.SET_SELECTED_PLACE_DATA: {
      return {
        ...state,
        selectedPlaceData: action.selectedPlaceData
      }
    }

    /** API */
    case actionTypes.API_ERROR: {
      return {
        ...state,
        errorMsg: action.errorMsg
      }
    }

    /** Default */
    case actionTypes.RESET_REDUX_DATA: {
      return initialState
    }
    default:
      return initialState
  }
}
