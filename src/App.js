import React from "react";
import "./App.css";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import API from './util/api'
import _ from "lodash";
import { updateHistoryList, resetReduxData, setSelectedPlace, setApiKey, apiError } from "./redux/actions";
import { connect } from 'react-redux';
import Hoc from './Hoc'

@connect((state) => ({
  historyList: state.historyList,
  selectedPlaceData: state.selectedPlaceData,
  api_key: state.api_key
}), {
  updateHistoryList, resetReduxData, setSelectedPlace, setApiKey, apiError
})
class GoogleMap extends React.Component {
  constructor(props) {
    super(props);
    this.autocompleteTf
    this.map

    this.state = {
      predictions: this.props.historyList,
      isShowAlert: true,
      value: ''
    }
  }

  componentDidMount() {
    // this.props.resetReduxData() 
    this.initialSetup()
  }

  initialSetup = () => {
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyCS2skggXWdR4tAYQWFquK1Bi6IcAhaEJw&libraries=places&callback=initMap`;
    window.initMap = this.initMap.bind(this);
    document.body.appendChild(script);
  }
  initMap() {
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: -33.8688, lng: 151.2195 },
      zoom: 13
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedPlaceData !== this.props.selectedPlaceData) {
      this.pointToLocationOnMap(this.props.selectedPlaceData)
    }
  }

  onInputChange = async (e, value) => {
    this.setState({ value })
    if (!value) { return }
    try {
      let data = await API.getSuggestion({ input: value })
      this.setState({
        predictions: data.predictions
      })
    }
    catch (e) {
      console.log(e)
      let msg = e.errorMsg ? e.errorMsg : e
      this.props.apiError(msg)
    }
  }

  onChange = async (e, value) => {
    if (!value) { return }
    this.props.setSelectedPlace(value)
    this.addHistoryList(value)
  }
  addHistoryList = (value) => {
    // Remove duplicate, and append at start of array
    let temp = this.props.historyList
    temp = temp.filter(d => d.id != value.id)
    temp.unshift(value)
    this.props.updateHistoryList(temp)
  }

  onAPIChange = (e) => {
    this.props.setApiKey(e.target.value)
  }

  pointToLocationOnMap = (place) => {
    var marker = new google.maps.Marker({
      map: this.map,
      anchorPoint: new google.maps.Point(0, -29)
    });
    var bounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(place.geometry.viewport.northeast.lat, place.geometry.viewport.northeast.lng),
      new google.maps.LatLng(place.geometry.viewport.southwest.lat, place.geometry.viewport.southwest.lng),
    );
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(new google.maps.LatLng(place.geometry.viewport.northeast.lat, place.geometry.viewport.northeast.lng));
    bounds.extend(new google.maps.LatLng(place.geometry.viewport.southwest.lat, place.geometry.viewport.southwest.lng));
    this.map.fitBounds(bounds)

    this.map.setCenter(place.geometry.location);
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
  }



  render() {
    return (
      <div className="search-location-input">
        <div id="text-fields">
          <Autocomplete
            ref={ref => this.autocompleteTf = ref}
            onInputChange={this.onInputChange}
            id="combo-box-demo"
            options={this.state.value ? this.state.predictions : this.props.historyList}
            onChange={this.onChange}
            getOptionSelected={(option, value) => option.id === value.id}
            getOptionLabel={(option) => option.description}
            style={{ width: 300 }}
            inputValue={this.state.value}
            renderInput={(params) => <TextField {...params} label="Places" variant="outlined" />}
          />
          <TextField label="Input API key" value={this.props.api_key} variant="outlined" onChange={this.onAPIChange} />
        </div>
        <div id="map"></div>
      </div>
    );
  }
}

export default Hoc(GoogleMap);
